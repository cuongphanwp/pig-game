/*
Challenge:

1. hit 6 in a row, get 0 , and next player
2. Add input field to set the score to win
3. Add one more dice, loose current score when one of them hit 1

*/

var scores, roundScore, activePlayer, dice, gamePlaying;

init();






document.querySelector('.btn-roll').addEventListener('click', function () {
    
    if (gamePlaying) {
        //1. Random Number
        dice = Math.floor(Math.random() * 6) + 1;
        //2. Display the number
        var diceDOM =  document.querySelector('.dice');
        diceDOM.style.display = 'block';
        diceDOM.src = 'dice-' + dice + '.png';
    
        //3. Update score if the rolled number was not 1
        if (dice === 6 && lastDice === 6) {
            //lose his score
            scores[activePlayer] = 0;
            document.querySelector('#score-' + activePlayer).textContent = '0';
            nextPlay();
        } else if (dice !== 1) {
            //Add score
            roundScore += dice;
            document.querySelector('#current-' + activePlayer).textContent = roundScore;
        } else {
            nextPlay();
        }
        
       
        lastDice = dice;
    }
    
    
});


document.querySelector('.btn-hold').addEventListener('click', function () {
    
    if (gamePlaying) {
        //Add current score to global score
    scores[activePlayer] += roundScore;
    
    
    
    // Update the UI
    
    document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];
    
   
    
    
    //Check if the player won
    
    if (scores[activePlayer] >= 100) {
        
        document.querySelector('#name-' + activePlayer).textContent = 'You Won!';
        document.querySelector('.dice').style.display = 'none';
        document.querySelector('.player-' + activePlayer +'-panel').classList.add('winner');
        document.querySelector('.player-' + activePlayer +'-panel').classList.remove('active');
        
        gamePlaying = false;
        
    } else {
        nextPlay();
    }
    }
    
});


document.querySelector('.btn-new').addEventListener('click', init);

function init() {
    scores = [0,0];
    roundScore = 0;
    activePlayer =0;
    gamePlaying = true;
    
    document.querySelector('.dice').style.display = 'none';

    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');
    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.add('active');
}


function nextPlay () {
   //Next player
        activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
        roundScore = 0;
        
        document.getElementById('current-0').textContent ='0';
        document.getElementById('current-1').textContent ='0';
        
        document.querySelector('.player-0-panel').classList.toggle('active');
        document.querySelector('.player-1-panel').classList.toggle('active');
        
//        document.querySelector('.player-0-panel').classList.remove('active');
//        document.querySelector('.player-1-panel').classList.add('active');
        
        document.querySelector('.dice').style.display = 'none';
}













